﻿/* creación de la base de datos */

DROP DATABASE IF EXISTS WoW;
CREATE DATABASE WoW;

USE WoW;

/* crear las tablas */

  -- Razas

  CREATE OR REPLACE TABLE razas(
    id int AUTO_INCREMENT,
    nombre varchar(50),
    afiliacion varchar(50),
    descripcion varchar(200) ,
    racial varchar(50),
    PRIMARY KEY (id)
  );

-- Clases

  CREATE OR REPLACE TABLE clases(
    id int AUTO_INCREMENT,
    nombre varchar(50),
    descripcion varchar(50),
    atributosR varchar(200) ,
    talentosR varchar(50),
    PRIMARY KEY (id)
  );

-- Roles
  CREATE OR REPLACE TABLE Roles(
    id int AUTO_INCREMENT,
    id_clases int ,
    roles varchar(20),
    PRIMARY KEY(id)
  );

  -- Expansiones

  CREATE OR REPLACE TABLE expansiones(
    id int AUTO_INCREMENT,
    nombre varchar(50),
    rango_niveles varchar(50),
    fecha date ,
    PRIMARY KEY (id)
  );

  

-- Zonas

  CREATE OR REPLACE TABLE zonas(
    id int AUTO_INCREMENT,
    nombre varchar(50),
    num_regiones int,
    id_expansiones int,
    PRIMARY KEY (id)
  );

  -- Protagonistas

  CREATE OR REPLACE TABLE protagonistas(
    id int AUTO_INCREMENT,
    nombre varchar(50),
    raza varchar(50),
    afiliacion varchar(200) ,
    estado varchar(50),
    descripcion varchar(200),
    PRIMARY KEY (id)
  );

  -- Mazmorras

  CREATE OR REPLACE TABLE mazmorras(
    id int AUTO_INCREMENT,
    nombre varchar(50),
    ubicacion varchar(50),
    num_boses int,
    nivel_requerido int,
    id_expansiones int,
    PRIMARY KEY (id)
  );

  -- Pertenecen

    CREATE OR REPLACE TABLE pertenecen(
    id int AUTO_INCREMENT,
    id_razas int,
    id_expansiones int,
    PRIMARY KEY (id)
  );

    -- Tienen

    CREATE OR REPLACE TABLE tienen(
    id int AUTO_INCREMENT,
    id_razas int,
    id_clase int,
    PRIMARY KEY (id)
  );

    -- Aparecen

    CREATE OR REPLACE TABLE aparecen(
    id int AUTO_INCREMENT,
    id_protagonistas int,
    id_expansiones int,
    PRIMARY KEY (id)
  );

/** creacion de las restricciones **/

-- Roles

ALTER TABLE Roles
  ADD CONSTRAINT fk_clases_roles
  FOREIGN KEY (id_clases)
  REFERENCES Roles(id),

  ADD CONSTRAINT uk_clases_roles
  UNIQUE KEY (id_clases,roles);

-- Zonas

ALTER TABLE zonas
  ADD CONSTRAINT fk_expansiones_zonas
  FOREIGN KEY (id_expansiones)
  REFERENCES zonas(id);

-- Mazmorras

ALTER TABLE mazmorras
  ADD CONSTRAINT fk_expansiones_mazmorras
  FOREIGN KEY (id_expansiones)
  REFERENCES mazmorras(id);

-- Pertenecen

ALTER TABLE pertenecen
  ADD CONSTRAINT fk_expansiones_pertenecen
  FOREIGN KEY (id_expansiones)
  REFERENCES expansiones(id),

  ADD CONSTRAINT fk_razas_pertenecen
  FOREIGN KEY (id_razas)
  REFERENCES razas(id),

  ADD CONSTRAINT uk_expansiones_razas
  UNIQUE KEY (id_expansiones,id_razas);

-- Tienen

ALTER TABLE tienen
  ADD CONSTRAINT fk_razas_tienen
  FOREIGN KEY (id_razas)
  REFERENCES razas(id),

  ADD CONSTRAINT fk_clase_tienen
  FOREIGN KEY (id_clase)
  REFERENCES clases(id),

  ADD CONSTRAINT uk_clases_razas
  UNIQUE KEY (id_clase,id_razas);

-- Aparecen

ALTER TABLE aparecen
  ADD CONSTRAINT fk_expansiones_aparecen
  FOREIGN KEY (id_expansiones)
  REFERENCES expansiones(id),

  ADD CONSTRAINT fk_protagonistas_aparecen
  FOREIGN KEY (id_protagonistas)
  REFERENCES protagonistas(id),

  ADD CONSTRAINT uk_expansiones_protagonistas
  UNIQUE KEY (id_expansiones,id_protagonistas);
 
